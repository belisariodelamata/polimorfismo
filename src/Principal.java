/**
 *
 * @author BELSOFT
 */
public class Principal {

    public static void main(String[] args) {
        //////////////
        Persona persona = new Persona();
        System.out.println(persona);
        persona.comer();
        System.out.println("-----");
        
        Estudiante estudiante = new Estudiante();
        System.out.println(estudiante);
        estudiante.comer();
        System.out.println("-----");

        Docente docente = new Docente();
        docente.comer();
        System.out.println(docente);
        ///////////////

        System.out.println("-----");
        Persona personaE=estudiante;
        personaE.comer();
        
        IActividad actividad=(IActividad)estudiante;
        actividad.subir();
        
        Estudiante estudiante2=(Estudiante)personaE;
        System.out.println(estudiante2);
        System.out.println("----");        
        
        Estudiante estudianteOtro=new Estudiante();
        Persona personaPrueba=estudianteOtro;//new Persona();
        Estudiante estudiantePrueba=(Estudiante)personaPrueba;
    }
}
