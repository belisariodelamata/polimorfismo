
/**
 *
 * @author BELSOFT
 */
public class Docente extends Persona implements IComite, IActividad {

    @Override
    public void asistir() {
        System.out.println("Docente Asistir");
    }

    @Override
    public void subir() {
        System.out.println("Docente Subir Actividad");
    }

}
